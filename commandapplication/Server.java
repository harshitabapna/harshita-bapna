import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private Socket socket;
	private ServerSocket serverSocket;
	private DataInputStream input;
	private DataOutputStream output;

	public void startServer(int portNumber) throws IOException {

		serverSocket = new ServerSocket(portNumber);
		System.out.println("Server started");

		System.out.println("Waiting for a client ...");
	}

	public void acceptRequest() throws IOException {
		socket = serverSocket.accept();
		System.out.println("Client accepted");
	}

	public void receiveMessage() throws IOException {
		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
		String line = "";

		while (!line.equalsIgnoreCase("exit")) {
			line = input.readUTF();
			System.out.println(line);
			sendResponse(line);
		}
	}

	public void sendResponse(String line) throws IOException {
		if (!line.equalsIgnoreCase("exit")) {
			Process process = Runtime.getRuntime().exec(line);
			BufferedReader processRead = new BufferedReader(new InputStreamReader(process.getInputStream()));

			String command = null;
			String response = "";
			while ((command = processRead.readLine()) != null) {

				response = response + command + "\n";

			}
			output.writeUTF(response);
			output.flush();
		}
		
	}

	public void stopServer() throws IOException {
		socket.close();
		input.close();
	}

	public static void main(String args[]) throws IOException {
		Server server = new Server();
		server.startServer(5000);
		server.acceptRequest();
		server.receiveMessage();
		server.stopServer();

	}
}