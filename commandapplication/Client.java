import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client {
	private Socket socket;
	private DataInputStream input;
	private DataOutputStream output;

	public void connectClientServer(String ipAddress, int portNumber)
			throws UnknownHostException, IOException,ClassNotFoundException 
	{
        try {
		socket = new Socket(ipAddress, portNumber);
        }catch(SocketException e) {
        	System.out.println("server is not connected");
        }
	
	}

	public void closeConnection() throws IOException {
		input.close();
		output.close();
		socket.close();
	}

	public void sendRequest() throws IOException, ClassNotFoundException {
		
		input = new DataInputStream(System.in);
		output = new DataOutputStream(socket.getOutputStream());

		
		
		String command = "";
		while (!command.equalsIgnoreCase("exit")) {
			System.out.println("Type a command or type exit to end ");
			command = input.readLine();
			output.writeUTF(command);
			receiveServerResponse();
		}
	}

	public void receiveServerResponse() throws IOException, ClassNotFoundException {
		DataInputStream input = new DataInputStream(socket.getInputStream());
		String response = (String) input.readUTF();
		System.out.println("Server message: " + response);
	}

	public static void main(String args[]) throws IOException, ClassNotFoundException {
		
		Client client = new Client();
		client.connectClientServer("localhost", 5000);

		client.sendRequest();
		client.closeConnection();
	}
}