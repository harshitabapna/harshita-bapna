import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class Client {
	private Socket socket;
	private DataInputStream input;
	private DataOutputStream output;

	public void connectClientServer(String ipAddress, int portNumber)
			throws UnknownHostException, IOException, ClassNotFoundException {

		socket = new Socket(ipAddress, portNumber);
		System.out.println("Connected");
	}

	public void closeConnection() throws IOException {
		input.close();
		output.close();
		socket.close();
	}

	public void sendingRequest() throws IOException {
		input = new DataInputStream(System.in);
		output = new DataOutputStream(socket.getOutputStream());

		String line = "";
		while (!line.equalsIgnoreCase("exit")) {
			line = input.readLine();
			output.writeUTF(line);
		}
	}

	public void receiveServerResponse() throws IOException, ClassNotFoundException {
		ObjectInputStream in = new ObjectInputStream(socket.getInputStream());
		String response = (String) in.readObject();
		System.out.println("Server message: " + response);
	}

	public static void main(String args[]) throws IOException, ClassNotFoundException {
		Client client = new Client();
		client.connectClientServer("localhost", 5000);
		client.sendingRequest();
		client.receiveServerResponse();
		client.closeConnection();
	}
}