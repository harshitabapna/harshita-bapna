import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

	private Socket socket;
	private ServerSocket serverSocket;
	private DataInputStream input;
	private ObjectOutputStream output;

	public void startServer(int portNumber) throws IOException {

		serverSocket = new ServerSocket(portNumber);
		System.out.println("Server started");

		System.out.println("Waiting for a client ...");
	}

	public void acceptRequest() throws IOException {
		socket = serverSocket.accept();
		System.out.println("Client accepted");
	}

	public void receiveMessage() throws IOException {
		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));

		String line = "";

		while (!line.equalsIgnoreCase("exit")) {
			line = input.readUTF();
			System.out.println(line);
		}
	}

	public void sendResponse() throws IOException {
		output = new ObjectOutputStream(socket.getOutputStream());
		String response = "Connection Close";
		output.writeObject(response);
	}

	public void stopServer() throws IOException {
		socket.close();
		input.close();
	}

	public static void main(String args[]) throws IOException {
		Server server = new Server();
		server.startServer(5000);
		server.acceptRequest();
		server.receiveMessage();
		server.sendResponse();
		server.stopServer();

	}
}