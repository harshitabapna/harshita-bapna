import random
def roll(maxnumber):
    result=random.randint(1, maxnumber)
    return result


def main():
    maxnumber=6
    isRoll=True
    while isRoll:
        exitRolling=input("Ready to roll? Enter Q to Quit")
        if exitRolling.lower() !="q":
            result=roll(maxnumber)
            print("You have rolled a",result)
        else:
            isRoll=False