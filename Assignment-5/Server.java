package connection;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
	
public class Server {

	private Socket socket;
	private ServerSocket serverSocket;
	private DataInputStream input;
	private DataOutputStream output;
	private File file = null;
	private JSONArray jsonArray = new JSONArray();
	private FileReader fileReader = null;
	private FileWriter fileWriter = null;
	private Object object = null; 
	private JSONParser jsonParser = new JSONParser();
	private JSONObject jsonObject = new JSONObject();


	public void startServer(int portNumber) throws IOException {

		serverSocket = new ServerSocket(portNumber);
		System.out.println("Server started");

		System.out.println("Waiting for a client ...");
	}

	public void acceptRequest() throws IOException {
		socket = serverSocket.accept();
		System.out.println("Client accepted");
	}

	public void receiveMessage() throws Exception {
		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));

		String userData = (String) input.readUTF();
		checkCondition(userData);

	}

	public void checkCondition(String userData) throws Exception {
		JSONObject jsonObject = new JSONObject();
		jsonObject = (JSONObject) jsonParser.parse(userData);
		if (jsonObject.get("command").equals("SET")) {
			setData(jsonObject);
		} else if (jsonObject.get("command").equals("GET")) {
			getData(jsonObject);
		}
	}

	public  void getData(JSONObject jsonObject) throws Exception {
		String value="";
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject_i = (JSONObject) jsonArray.get(i);
			if (jsonObject.get("key").equals(jsonObject_i.get("key"))) {
				value = jsonObject_i.get("value").toString();
				sendResponse(value);
				break;
			}
			
		}
		
	}

	public void updateFile(JSONArray jsonArray) throws IOException {
	
		fileWriter = new FileWriter("C:\\Users\\harshita.bapna\\Desktop\\test.json");
        fileWriter.write(jsonArray.toJSONString());
		fileWriter.flush();
	}

	public void setData(JSONObject jsonObject) throws IOException {
		boolean flag = true;

		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject_i = (JSONObject) jsonArray.get(i);
			if (jsonObject.get("key").equals(jsonObject_i.get("key"))) {
				sendResponse("You are not allowed to enter the duplicate key");
				flag = false;
				break;
			}
		}
		if (flag) {
			jsonArray.add(jsonObject);
			updateFile(jsonArray);
			sendResponse("OK");
		}
		
	}

	public void readFromFile() throws Exception {

		file = new File("C:\\Users\\harshita.bapna\\Desktop\\test.json");
		if (file.exists() && file.length() != 0) {
			fileReader = new FileReader("C:\\Users\\harshita.bapna\\Desktop\\test.json");
			//System.out.println(fileReader);
			object = jsonParser.parse(fileReader);
			jsonArray = (JSONArray) object;
		}
	}

		

	public void sendResponse(String line) throws IOException {

		DataOutputStream data = new DataOutputStream(socket.getOutputStream());
		data.writeUTF(line);
		data.flush();

	}

	public void stopServer() throws IOException {
		socket.close();
		input.close();
	}

	public static void main(String args[]) throws Exception {
		Server server = new Server();
		server.startServer(5000);
		server.acceptRequest();
		server.readFromFile();
		server.receiveMessage();
		server.stopServer();

	}

}