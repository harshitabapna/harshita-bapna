package connection;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;

import org.json.simple.JSONObject;

public class Client {
	private Socket socket;
	private DataInputStream input = null;
	private DataOutputStream output = null;
	Scanner inputValue = new Scanner(System.in);
	static JSONObject jsonObject = new JSONObject();

	public void connectClientServer(String ipAddress, int portNumber)
			throws UnknownHostException, IOException,ClassNotFoundException 
	{
        try {
		socket = new Socket(ipAddress, portNumber);
		input = new DataInputStream(socket.getInputStream());
		output = new DataOutputStream(socket.getOutputStream());
        }
        catch(SocketException e) {
        	System.out.println("server is not connected");
        }
	
	}

	public void closeConnection() throws IOException {
		input.close();
		output.close();
		socket.close();
	}

	public void sendRequest() throws IOException, ClassNotFoundException {
		
		inputValuesFromUser();
        String userData = jsonObject.toJSONString();
  	    output.writeUTF(userData);
	    output.flush();
        receiveServerResponse();
        
		
	}
	public void inputValuesFromUser() {
		
		
        System.out.println("Enter Data");
        
        System.out.println("command: ");
        String command = inputValue.nextLine();
        jsonObject.put("command", command);

        System.out.println("key: ");
        String username = inputValue.nextLine();
        jsonObject.put("key", username);
        if(command.equals("SET")) {
            System.out.println("value: ");
            String value = inputValue.nextLine();
            jsonObject.put("value", value);
        } 
		
		
		
        }
        
	

	public void receiveServerResponse() throws IOException, ClassNotFoundException {
		DataInputStream input = new DataInputStream(socket.getInputStream());
		String response = (String) input.readUTF();
		System.out.println(response);
	}

	public static void main(String args[]) throws IOException, ClassNotFoundException{
		
		Client client = new Client();
		client.connectClientServer("localhost", 5000);

		client.sendRequest();
		client.closeConnection();
	}
}