package connection;

import static org.junit.Assert.*;

import org.json.simple.JSONObject;
import org.junit.Test;

public class ClientTest {

	@Test
	public void testInputValuesFromUser() {
	
		Client client = new Client();
		Client.jsonObject = new JSONObject();
		JSONObject expectedJsonObject = new JSONObject();
		expectedJsonObject.put("command","harshi" );
	    expectedJsonObject.put("key","harshi99");
		client.inputValuesFromUser();
		assertEquals(expectedJsonObject, client.jsonObject);
}

	@Test
	public void testInputValuesFromUserIsSet() {
	
		Client client = new Client();
		Client.jsonObject = new JSONObject();
		JSONObject expectedJsonObject = new JSONObject();
		expectedJsonObject.put("command","SET" );
	    expectedJsonObject.put("key","harshi");
	    expectedJsonObject.put("value","harshi99");
		client.inputValuesFromUser();
		assertEquals(expectedJsonObject, client.jsonObject);
}

	
}
