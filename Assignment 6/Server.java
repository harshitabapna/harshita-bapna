package connection;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Server {
	private Socket socket;
	private String command="";
	private ServerSocket serverSocket;
	private DataInputStream input;
	private DataOutputStream output;
	private JSONArray jsonArray1 = new JSONArray();
	private Object object = null; 
	private JSONParser jsonParser = new JSONParser();
	private JSONObject jsonObject = new JSONObject();
	
	public void startServer(int portNumber) throws IOException {
		serverSocket = new ServerSocket(portNumber);
		System.out.println("Server started");
		System.out.println("Waiting for a client ...");
	}
	public void acceptRequest() throws IOException {
		socket = serverSocket.accept();
		System.out.println("Client accepted");
	}
	public void receiveMessage() throws Exception
	{
		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        do 
        {
        	String userData = (String) input.readUTF();
        	jsonObject = (JSONObject) jsonParser.parse(userData);
        	command = jsonObject.get("command").toString();
        	GetAndSetValue();
        } while(!command.equalsIgnoreCase("exit"));
      }
	public void GetAndSetValue() throws Exception
	{
		if (jsonObject.get("command").equals("SET")) {
			setData(jsonObject);
		} else if (jsonObject.get("command").equals("GET")) {
			getData(jsonObject);
		}
	}
	public  void getData(JSONObject jsonObject)
	{
		String value="";
		for (int i = 0; i < jsonArray1.size(); i++) 
		{
			JSONObject jsonObject_i = (JSONObject) jsonArray1.get(i);
			if (jsonObject.get("key").equals(jsonObject_i.get("key"))) 
			{
				value = jsonObject_i.get("value").toString();
				try
				{
				sendResponse(value);
				} catch (IOException e)
				{
					System.out.println("sendResponse not worked");
				}
				break;
			}
		}
		
	}
	public void setData(JSONObject jsonObject) throws IOException 
	{
		boolean flag = true;
		jsonObject.remove("command");
		for (int i = 0; i < jsonArray1.size(); i++) 
		{
			JSONObject jsonObject_i = (JSONObject) jsonArray1.get(i);
			if (jsonObject.get("key").equals(jsonObject_i.get("key"))) 
			{
				sendResponse("You are not allowed to enter the duplicate key");
				flag = false;
				break;
			}
		}
		if (flag) {
			jsonArray1.add(jsonObject);
			System.out.println(jsonArray1);
			sendResponse("OK");
		}
		
	}
	public void sendResponse(String line) throws IOException
	{

		DataOutputStream data = new DataOutputStream(socket.getOutputStream());
		data.writeUTF(line);
		data.flush();
	}
	public void stopServer() throws IOException
	{
		socket.close();
		input.close();
	}
	public static void main(String args[]) throws Exception
	{
		Server server = new Server();
		server.startServer(5000);
		server.acceptRequest();
		//server.readFromFile();
		server.receiveMessage();
		server.stopServer();
	}

}