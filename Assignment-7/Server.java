package connection;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;
import java.net.Socket;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Server {
	private Socket socket;
	private ServerSocket serverSocket;
	public void startServer(int portNumber) throws IOException {
		serverSocket = new ServerSocket(portNumber);
		System.out.println("Server started");
		System.out.println("Waiting for a client ...");
	}

	public void acceptRequest() throws IOException {
		int counter=0;
		while(true) {
			counter++;
			socket = serverSocket.accept();
			System.out.println("Client accepted");
			System.out.println(" >> " + "Client No:" + counter + " started!");
			ServerClientThread sct=new ServerClientThread(socket,counter);
	        sct.start();
		}
	}
	public static void main(String args[]) throws Exception {
		Server server = new Server();
		server.startServer(5000);
		server.acceptRequest();
	  
	}

}