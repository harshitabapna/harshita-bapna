package connection;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

class ServerClientThread extends Thread {
	int clientNo;
	private Socket socket;
	private String command = "";
	private ServerSocket serverSocket;
	private DataInputStream input;
	private DataOutputStream output;
	private JSONArray jsonArray1 = new JSONArray();
	private Object object = null;
	private JSONParser jsonParser = new JSONParser();
	private JSONObject jsonObject = new JSONObject();
	long totalTime;
	int expiredTime;
	long currentTime;


	ServerClientThread(Socket inSocket,int counter) throws IOException{
		this.socket = inSocket;
		this.clientNo=counter;
		input = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
		output = new DataOutputStream(new BufferedOutputStream(socket.getOutputStream()));
	}
	public void receiveMessage() throws Exception {


		do {
			String userData = (String) input.readUTF();
			jsonObject = (JSONObject) jsonParser.parse(userData);
			command = jsonObject.get("command").toString();
			GetAndSetValue();
		} while (!command.equalsIgnoreCase("exit"));
	}

	public void GetAndSetValue() throws IOException {
		if (jsonObject.get("command").equals("SET")) {
			setData(jsonObject);
		} else if (jsonObject.get("command").equals("GET")) {
			getData(jsonObject);
		}
	}

	public void getData(JSONObject jsonObject) throws IOException {

		int flag=1;
		for (int i = 0; i < jsonArray1.size(); i++) {
			JSONObject jsonObject_i = (JSONObject) jsonArray1.get(i);
			String value = "";
			if (jsonObject.get("key").equals(jsonObject_i.get("key"))) {
				if(Long.parseLong(jsonObject_i.get("ttl").toString()) > System.currentTimeMillis()) {
					flag=0;
					value = jsonObject_i.get("value").toString();
					sendResponse(value);
					break;
				}

			}
			else
				sendResponse("Key not found");
		}
		if(flag==1)
		{
			sendResponse("Time has expired");
		}

	}


	public void setData(JSONObject jsonObject) throws IOException {
		boolean flag = true;
		currentTime = System.currentTimeMillis();
		expiredTime = Integer.parseInt(jsonObject.get("ttl").toString());
		totalTime=currentTime+(expiredTime*1000);
		System.out.println(totalTime);
		jsonObject.put("ttl", totalTime);
		for (int i = 0; i < jsonArray1.size(); i++) {

			JSONObject jsonObject_i = (JSONObject) jsonArray1.get(i);
			if (jsonObject.get("key").equals(jsonObject_i.get("key"))) {
				if(jsonObject.get("value").equals(jsonObject_i.get("value"))){
					flag=false;
					sendResponse("You are entering the same key with same value");
				}
				else {
					jsonObject_i.put("value",jsonObject.get("value"));
				}

			}

		}

		if (flag) {
			jsonArray1.add(jsonObject);
			System.out.println(jsonArray1);
			try {
				sendResponse("OK");
			} catch (IOException e) {
				System.out.println("Response not found");
			}
		}

	}

	public void sendResponse(String line) throws IOException {


		output.writeUTF(line);
		output.flush();
	}

	public void stopServer() {
		try {
			socket.close();
			input.close();
		} catch (IOException e) {
			System.out.println("connection not closed");
		}
	}
	public void run(){
		try{
			receiveMessage();
			stopServer();
		}catch(Exception ex){
			ex.printStackTrace();
		}finally{
			System.out.println("Client -" + clientNo + " exit!! ");
		}
	}
}