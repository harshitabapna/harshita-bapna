package connection;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;
import org.json.simple.JSONObject;

public class Client {
	private Socket socket;
	private DataInputStream input = null;
	private DataOutputStream output = null;
	Scanner inputValue = new Scanner(System.in);
	int timeToLive;
	static JSONObject jsonObject = new JSONObject();

	public void connectClientServer(String ipAddress, int portNumber)
			throws UnknownHostException, IOException,ClassNotFoundException 
	{
		try {
			socket = new Socket(ipAddress, portNumber);
			input = new DataInputStream(socket.getInputStream());
			output = new DataOutputStream(socket.getOutputStream());
		}
		catch(SocketException e) {
			System.out.println("server is not connected");
		}
	}
	public void closeConnection() throws IOException {
		input.close();
		output.close();
		socket.close();
	}
	public void sendRequest() throws IOException, ClassNotFoundException {
		do {
			inputValuesFromUser();
			String userData = jsonObject.toJSONString();
			output.writeUTF(userData);
			output.flush();
			receiveServerResponse();
		}while(!jsonObject.get("command").toString().equalsIgnoreCase("exit"));

	}
	public void inputValuesFromUser() {
		String command = inputValue.nextLine();
		jsonObject.put("command", command);
		String username = inputValue.nextLine();
		jsonObject.put("key", username);
		if(command.equalsIgnoreCase("SET")) {
			String value = inputValue.nextLine();
			jsonObject.put("value", value);
			timeToLive = inputValue.nextInt();
			inputValue.nextLine();
			jsonObject.put("ttl",timeToLive);
		} 

	}
	public void receiveServerResponse() throws IOException, ClassNotFoundException {
		String response = (String) input.readUTF();
		System.out.println(response);
	}
	public static void main(String args[]) throws IOException, ClassNotFoundException{

		Client client = new Client();
		client.connectClientServer("localhost", 5000);
		client.sendRequest();
		client.closeConnection();
	}
}